# Bungenix-UI Configs

This project provides different example configurations for BungeniX-UI. 

These need to be symlinked into the `./src` folder of `bungenix-ui`. 

For example if you have `bungenix-ui` and `bungenix-ui-configs` in the same folder level, then to symlink `bungenix-ui-configs/bungeni-ui-config-02`:

```
cd bungenix-ui/src
ln -s ../../bungenix-ui-configs/bungenix-ui-config-02 ./bungenix-ui-config
```



