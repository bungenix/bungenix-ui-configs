export function setUpProxy() {
    console.log("Setting up proxies");
    window.gawati = {
        GAWATI_PROXY: "http://data.local",
        GAWATI_DOCUMENT_SERVER: "http://media.local",
        GAWATI_PROFILE_SERVER: "http://localhost"
  };
}

export const getFilter = () => {
    return {
        'filterPath':'Filter2',
        'enabledFilters': ['years', 'countries', 'langs', 'keywords', 'types']
    };
}

export const filterConfigProxy = {
    'countries': {
      xqueryElementXPath: './/an:FRBRcountry',
      xqueryAttr: '@value',
      xqueryAttrType: 'string'
    },
    'langs': {
      xqueryElementXPath: './/an:FRBRlanguage',
      xqueryAttr: '@language',
      xqueryAttrType: 'string'
    },
    'years': {
      xqueryElementXPath: './/an:FRBRExpression/an:FRBRdate',
      xqueryAttr: 'year-from-date(@date)',
      xqueryAttrType: 'int'
    },
    'keywords': {
      xqueryElementXPath: './/an:classification/an:keyword',
      xqueryAttr: '@value',
      xqueryAttrType: 'string'
    },
    'types': {
      xqueryElementXPath: ['.//an:act','.//an:amendment','.//an:amendmentList','.//an:bill','.//an:debate','.//an:debateReport','.//an:doc','.//an:documentCollection','.//an:judgment','.//an:officialGazette','.//an:portion','.//an:statement'],
      xqueryAttr: '@name',
      xqueryAttrType: 'string'
    },
    'themes': {
      xqueryElementXPath: './/an:references/an:TLCConcept',
      xqueryAttr: '@showAs',
      xqueryAttrType: 'string'
    }
};
