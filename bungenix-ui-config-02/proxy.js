export function setUpProxy() {
    console.log("Setting up proxies");
    window.bungenix = {
        BUNGENIX_PROXY: process.env.REACT_APP_BUNGENIX_PROXY || "http://ecowasdata.local",
        BUNGENIX_DOCUMENT_SERVER: process.env.REACT_APP_BUNGENIX_DOCUMENT_SERVER || "http://ecowasmedia.local",
        BUNGENIX_PROFILE_SERVER: "http://localhost"
  };
}

export const getFilter = () => {
    return {
        'filterPath':'Filter2',
        'enabledFilters': ['volumes', 'years', 'langs', 'types']
   }
}

export const filterConfigProxy = {
    'countries': {
      xqueryElementXPath: './/an:FRBRcountry',
      xqueryAttr: '@value',
      xqueryAttrType: 'string'
    },
    'langs': {
      xqueryElementXPath: './/an:FRBRlanguage',
      xqueryAttr: '@language',
      xqueryAttrType: 'string'
    },
    'years': {
      xqueryElementXPath: './/an:FRBRExpression/an:FRBRdate',
      xqueryAttr: 'year-from-date(@date)',
      xqueryAttrType: 'int'
    },
    'keywords': {
      xqueryElementXPath: './/an:classification/an:keyword',
      xqueryAttr: '@value',
      xqueryAttrType: 'string'
    },
    'types': {
      xqueryElementXPath: ['.//an:act','.//an:amendment','.//an:amendmentList','.//an:bill','.//an:debate','.//an:debateReport','.//an:doc','.//an:documentCollection','.//an:judgment','.//an:officialGazette','.//an:portion','.//an:statement'],
      xqueryAttr: '@name',
      xqueryAttrType: 'string'
    },
    'themes': {
      xqueryElementXPath: './/an:references/an:TLCConcept',
      xqueryAttr: '@showAs',
      xqueryAttrType: 'string'
    },
    'volumes': {
      xqueryElementXPath: './/an:FRBRnumber',
      xqueryAttr: '@value',
      xqueryAttrType: 'string'
    },
    'volumes_statement': {
      xqueryElementXPath: './/an:statement/an:meta/an:proprietary/gw:gawati/gw:partOfCollection',
      xqueryAttr: '@showAs',
      xqueryAttrType: 'int'
    },
};